const bcrypt = require('bcrypt');
const saltRounds = 10; //Changing it later in hashSync Function for number of cipher Rounds

//Función hash.... Cifrado de datos de entrada y numero de pasadas de cifrado (Rounds)
function hash(data) {
    console.log("Hashing data");
    return bcrypt.hashSync(data, 10); //Indicamos lo que cifrar y 10 pasadas de cifrado

}

//Verificación de password en modo texto y en hash.
function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
    console.log("checking password");

    return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed)
}
//Exportamos las dos funciones del modulo
module.exports.hash = hash;
module.exports.checkPassword = checkPassword;