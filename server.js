/* **************************************************************************************                         
                                START: Environment Initialization
 ****************************************************************************************

  INFO: Require: (dotenv)
        -> Required for charging environment variables inside the .env file to the  process.env 
        -> .env file content -> IDKey for connecting MLAB-->APIKEY. 
  INFO: Express: Charging Express and initialization to --> "app" variable.
  INFO: Express.json(): Parsing JSON requests. 
  INFO: enableCORS: Configuration for access connections between different Domains
  
*/

require('dotenv').config();
const express = require('express');
const app = express();
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

//INFO: CORS
var enableCORS = function(req, res, next) {
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        res.set("Access-Control-Allow-Headers", "Content-Type");
        next();
    }

//INFO:  Boot Server and Port configuration.
app.use(express.json()); //Parses json requests.
app.use(enableCORS);
const port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto BIP: " + port);

/****************************************************************************************                         
                                END: Environment Initialization
 ****************************************************************************************
*/

//Loading user, accounts and authentication controllers. 
const userController = require('./controllers/userController');
const authController = require('./controllers/authController');
const accountController = require('./controllers/accountController');

//****-----APP.GET------*********
app.get('/apitechu/testnode', userController.testnode);
app.get('/apitechu/users', userController.getUsers);
app.get('/apitechu/users/:id', userController.getUsersById);
app.get('/apitechu/accounts/:id', accountController.getAccountById);
app.get('/apitechu/listmov/:id', accountController.listMov);

//****-----APP.POST------*********
app.post('/apitechu/users', userController.createUsers);
app.post('/apitechu/login', authController.login);
app.post('/apitechu/logout/:id', authController.logout);
app.post('/apitechu/newmov', accountController.newMov);
app.post('/apitechu/transfermov', accountController.transferMov);

