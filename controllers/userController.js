/****************************************************************************************                         
                                START: Environment Initialization
 ***************************************************************************************

INFO: @requestjson -> Simplifying JSON requests.
INFO: @baseMLABURL -> URL for the connection to MLAB
INFO: @mLabAPIKey -> APIKEY for the connection to MLAB.
INFO: @crypt -> Charging crypt for the creation of user passwords creation (hashed) */

const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjb9ed/collections/";
const crypt = require('../crypt');
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

/****************************************************************************************                         
                                END: Environment Initialization
****************************************************************************************/


/****************************************************************************************                         
                                START: FUNCTIONS
 ***************************************************************************************/
//Testing Node is up and running.
function testnode(req, res) {
    console.log("GET /apitechu/Test");
    res.send({ "backNode-msg": "Servidor activo y a la escucha" });
}


// ***************************************
// //****-----GET FUNCTIONS------*********
// ***************************************

/*Function @getUsers Get all users from the user Collection in MLAB and returns: 
        OUTPUT: 
        @"id"
        @"first_name": FIRST_NAME
        @"last_name": LAST_NAME
        @"email": EMAIL
        @"sexo": "M" OR "H"
        @"password": Cipher hash
*/

function getUsers(req, res) {

    console.log('GET apitechu/GetUsers');
    // @httpClient -> Client Connection to the database.
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('Conexión cliente creada a:\n ', baseMlabURL);
    //Getting the user information to the DB.
    httpClient.get("user?" + mLabAPIKey,
        function(err, resMLAB, body) {

            var response = !err ? body : { "message:": "Error obteniendo usuarios" };
            res.send(response);
        });
}

/*Function @getUsersById. Get the user /:id from the user Collection in MLAB and returns: 
        @"id"
        @"first_name": FIRST_NAME
        @"last_name": LAST_NAME
        @"email": EMAIL
        @"sexo": "M" OR "H"
        @"password": Cipher hash
*/


function getUsersById(req, res) {
    console.log('GET /apitechu/users/:id');
    // @httpClient -> Client Connection
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('Conexión cliente creada a:\n ', baseMlabURL);

    //Store the params.id received into @id
    var id = req.params.id;
    console.log("El id del usuario solicitado es: " + id);

    //Setting the MLAB Query. 
    var query = 'q={"id":' + id + '}'
    console.log("El valor de query es: " + query);
    console.log("Query: " + baseMlabURL + "user?" + query + mLabAPIKey);

    //Getting the user information from MLAB
    httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLAB, body) {
           
        //Callback treatment for the query.

            //In case of system error  
            if (err) {
                console.log("Error de sistema al obtener el usuario");
                var response = {
                    "msg": "Error de sistema al obtener el usuario"
                }
                res.status(500);
            //In case of getting the user information.
            } else {
                console.log("Datos del usuario obtenidos.")
                if (body.length > 0) {
                    var response = body[0];
                    res.status(200);
            //In case of user nos found
                } else {
                    var response = {
                        "msg": "Usuario no encontrdo en DB"
                    }
                    res.status(404);
                }

            }
            res.send(response);
        });
};

// ***************************************
// //****-----POST FUNCTIONS------*********
// ***************************************

/*FUNCTION @createUsers. Create the user account into the user Collection in MLAB : 

        INPUT: 
        @"id"
        @"first_name": FIRST_NAME
        @"last_name": LAST_NAME
        @"email": EMAIL
        @"sexo": "M" OR "H"
        @"password": Cipher hash
        RETURNS: 
        Confirmation of the user creation in database.
*/

function createUsers(req, res) {
    //Testing we receive the right parameters from req.body
    console.log("POST /apitechu/users");
    console.log("req.body.id");
    console.log("req.body.first_name");
    console.log("req.body.last_name");
    console.log("req.body.email");
    console.log("req.body.password");

    //Creating the new user JSON variable.
    var newUser = {
        "id": req.body.id,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "sexo": req.body.sexo,
        //Saving ciphered password -> "clearedpassword" to -> req.body.password,
        "password": crypt.hash(req.body.password)
    };

    //Creating the connecion to the database and inserting the new user to MLAB User DB.
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('Conexión cliente creada a:\n ', baseMlabURL);
    //Insertamos los datos. 
    httpClient.post("user?" + mLabAPIKey, newUser,
        function(err, resMLAB, body) {
            console.log("El post a enviar es: user?" + mLabAPIKey + newUser);
            console.log("usuario almacenado en BD correctamente.");
            res.status(201).send({ "msg": "Usuario almacenado en BD correctamente." })
        });


}


/****************************************************************************************                         
                                END: FUNCTIONS
 ***************************************************************************************/


module.exports.testnode = testnode; //Testing Node Connection
module.exports.getUsers = getUsers; //Get UsersDB
module.exports.getUsersById = getUsersById; //Get Specific UserDB by ID.
module.exports.createUsers = createUsers;




