/****************************************************************************************                         
                                START: Environment Initialization
 ****************************************************************************************
// INFO: @requestjson -> Simplifying JSON requests.
// INFO: @baseMLABURL -> URL for the connection to MLAB
// INFO: @mLabAPIKey -> APIKEY for the connection to MLAB.
***************************************************************************************/
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjb9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
/****************************************************************************************                         
                                END: Environment Initialization
****************************************************************************************/

/****************************************************************************************                         
                                START: FUNCTIONS
 ***************************************************************************************/

 /*Function @getAccountById -> Getting all accounts from a user/:id
        INPUT: 
        Parameter: user:/id
      
        ACTIONS: 
        Get all accounts associated to a userid.
        RETURN:
        Returning  results of getAccountById.

*/
function getAccountById(req, res) {

    //console.log("GET /apitechu/accounts/:id");
    console.log("GET /apitechu/accounts/:id");


    //Creating the connection to the MLAB DB with httpClient(request-json)
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log(httpClient);
    console.log('Conexión cliente creada a:\n ', baseMlabURL);

    //Getting the input values param->user:/id 
    var id = req.params.id;
    console.log("El id del usuario indicado es: " + id);
    //Setting the query for the DB.
    var query = 'q={"user_id":' + id + '}';

    //Testing the query is right.  
    console.log("El valor de query es: " + query);
    console.log("La consulta completa es: " + baseMlabURL + "accounts?" + query + "&" + mLabAPIKey);
    
    //Checking the accounts owned by the userid.
    httpClient.get("accounts?" + query + "&" + mLabAPIKey,
        function(err, resMlab, body) {
            //Treatment for system error, getting the query to the DB (http 500)
            if (err) {
                var response = {
                    "msg": "Error de sistema obteniendo la cuenta"
                }
                res.status(500);
            //Treatment in case we can find accounts associated to the userid.
            } else {
                if (body.length > 0) {
                    var response = body;
            //Treatment in case there weren't accounts associated to the userid.
                } else {
                    var response = {
                        "msg": "Cuenta no encontrada",
                        "body": body
                    }
                    res.status(404);
                }
            }
            //Sending results.
            res.send(response);
        }
    )
};

 /*Function @newMov -> New movement
        INPUT: 
        @id -> Account id.
        @concept -> Concept of the movement -> Example: Send money or withdraw money.
        @amount -> Quantity of money involve in the operation.
        ACTIONS: 
        Get 
        RETURN:
        Returning  results of getAccountById.

*/
function newMov(req, res) {

    //CHECK: Checking we are inside the newMov function.
    console.log("POST /apitechu/newMov");
        
    //CHECK: Checking the values of input (BODY)
    console.log("Id de la cuenta: " + req.body.id);
    var concepto = req.body.concepto;
    console.log("Concepto: " + req.body.concepto);
    console.log("Amount: " + req.body.amount);

    //Additional data (Date() Operation)
    var fechaOperacion = new Date();
    console.log("Fecha Operación: " + fechaOperacion);

    //Creating the connection to the DB->  httpClient (request-json)
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log(httpClient);
    console.log('Conexión cliente creada a:\n ', baseMlabURL);
    var id = req.body.id;
    var amount = req.body.amount;
    var description = req.body.description;
    console.log("El id de la cuenta indicado es: " + id);
    var query = 'q={"id":' + id + '}';

    //Getting the accounts from the user: /:id
    httpClient.get("accounts?" + query + "&" + mLabAPIKey,
        function(err, resMlab, body) {
            //Treatment for system error, getting the query to the DB (http 500)
            if (err) {
                var response = {
                    "msg": "Error de sistema obteniendo la cuenta"
                }
                res.status(500);
            //Treatment with the accounts stored in the DB or NOT.
            } else {
                if (body.length > 0) {
                    var currentBalance = body[0].balance;
                    var newBalance;
                    if (amount >= 0) {
                        console.log("Cantidad a ingresar es positiva: Realizando Transferencia de: " + amount );
                        //console.log(body[0].balance);
                        newBalance = currentBalance + amount;
                    } else {
                        console.log("Cantidad a ingresar es negativa: Realizando retirada de fondos de:  " + amount );
                        newBalance = currentBalance + amount;
                    }
                    console.log("CurrentBalance: " + currentBalance);
                    console.log("UpdatedBalance: " + newBalance);
                    console.log('{"$set":{"balance":' + newBalance + '}}');

                    var putBody = '{"$set":{"balance":' + newBalance + '}}';
                    
                    httpClient.put("accounts?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                    function(err, resMLab, body) {
                        console.log(JSON.parse(putBody));
                        console.log("Balance actualizado con exito de: " + currentBalance + " a " + newBalance);
                        //console.log(body[0].balance);
                    }
                    )
                                       
                    //**************************************************************** */
                    //Saving the value to the DB-Collection @accounts_movements to register the history of movements. 
                    var postAccountsMovements =  '{"account_id":' + id + ',"fecha":"' + fechaOperacion + '","Concepto":"' + concepto + '","CantidadModificada":"' + amount  + '","BalanceAnterior":' + currentBalance + ',"BalanceActual":' + newBalance + '}' 

                    httpClient.post("accounts_movements?" +  "&" + mLabAPIKey, JSON.parse(postAccountsMovements),
                        function(err, resMLab, body) {
                            console.log(JSON.parse(putBody));
                    
                            //console.log(body[0].balance);
                        }
                    )

                    //Setting the response.
                    var response = {
                        "account_id": body[0].id,
                        "fecha": fechaOperacion,
                        "concepto": concepto,
                        "cantidadModificada": amount,
                        "iban": body[0].iban,
                        "user_id": body[0].user_id,
                        "old_balance": currentBalance,
                        "newBalance": newBalance

                    }
                } else {
                    var response = {
                        "msg": "Cuenta no encontrada",
                        "body": body
                    }
                    res.status(404);
                }
            }
            res.send(response);
            console.log(fechaOperacion);
        })




}

 /*Function @listMov
        INPUT: 
        @id_cuenta 
        ACTIONS: 
        Getting the historic data about account movements.
        RETURN:
        Returning  results of listMov.

*/
function listMov(req,res){

    //Getting the input values.
    var id_cuenta = req.params.id;
    //var id_cuenta = req.body.id_cuenta;
    console.log('GET apitechu/listMov/:id');
    console.log("El id de la cuenta indicada es " + id_cuenta );
    // @httpClient -> Client Connection DB.
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('Conexión a base de datos creada a:\n ', baseMlabURL);
    //Setting the query to the DB.
    var query = 'q={"account_id":' + id_cuenta + '}'
    console.log("El valor de query es: " + query);
    console.log("Query Completa: " + baseMlabURL + "account_id?" + query + "&" + mLabAPIKey);
    //Sending the DB request and getting the results.
    httpClient.get("accounts_movements?" + query + "&" + mLabAPIKey,
        function(err, resMLAB, body) {

            var response = !err ? body : { "message:": "Error obteniendo listado de cuentas" };
            res.send(response);
        });
    
}

/*Function @transferMov -> New Transfer 
        INPUT: 
        @account_id -> Account id.
        @ibanDestino -> Sending money to a Iban account
        @concepto -> Concept of the operation. 
        @cantidad -> Quantity of money involve in the operation.
        ACTIONS: 
        Transfering money for Client account to another account. 
            1. If the destiny IBAN ACCOUNT is from the same Bank, the transfer operation will be registered in both accounts. (Client and Destiny) 
            2. If the destiny IBAN ACCOUNT is from another Bank, the transfer operation  will discount the money in the client Account, and will send a message of Transfer Realized to the other bank account.
        RETURN:
        Returning  results of transferMov.

*/

function transferMov(req,res){

    //Getting and checking the Input values.     
    console.log("POST /apitechu/transferMov");
    
    var concepto = req.body.concepto;
    var amount = req.body.amount;
    var originAccount_id = req.body.account_id;
    var destinyAccountId;
    var ibanDestino = req.body.ibanDestino;
    var ibanOrigen;
    var originCurrentBalance;
    var originUpdatedBalance;
    var destinationCurrentBalance;
    var destinationUpdatedBalance;
    var destinyIbanFounded = false;
    var originAccountFounded = false; 
    var queryOriginAccount; 
    var queryDestinationAccount;
    
    
    //Additional data Operation Date();
    var fechaOperacion = new Date();
    console.log("Fecha Operación: " + fechaOperacion);
    
    //Creating the connection to the MLAB DB with httpClient(request-json)
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log(httpClient);
    console.log('Conexión cliente creada a:\n ', baseMlabURL);
    
// ******************************************************
//  Getting the information of the Origin client account.
// ******************************************************

    
    if(amount>=0){
        var queryDestinationAccount = 'q={"iban":"' + ibanDestino + '"}';
        console.log("El valor de query es: " + queryDestinationAccount);
        console.log("La consulta completa es: " + baseMlabURL + "accounts?" + queryDestinationAccount + "&" + mLabAPIKey);
                
        httpClient.get("accounts?" + queryDestinationAccount + "&" + mLabAPIKey,
            function(err, resMlab, body) {
                //Treatment for system error, getting the query to the DB (http 500)
               
                if (err) {
                    var response = {
                        "msg": "Error de sistema obteniendo informacion de la cuenta Destino"
                    }
                    res.status(500);
                //Treatment with the accounts stored in the DB or NOT.
                }else{
                    //If The IBAN account is from this entity. 
                    if (body.length > 0) {
                        
                        console.log("IBAN: " + ibanDestino + " encontrado. El IBAN destino pertenece a esta entidad. Tratando ambas cuentas.");
                        destinyAccountId = body[0].id;
                        var destinationCurrentBalance = body[0].balance;
                        destinyAccountId = body[0].id;
                                        
// **************************************************
//  Getting and updating the Origin account.
// **************************************************      

                    var queryOriginAccount = 'q={"id":' + originAccount_id + '}';

                    console.log("llegando al tratamiento de la cuenta de destino.");
                    console.log("llegando al tratamiento de la cuenta de destino.: Query-> " + queryOriginAccount);
                    
                    httpClient.get("accounts?" + queryOriginAccount + "&" + mLabAPIKey,
                        function(err, resMlab, body) {
                            //Treatment for system error, getting the query to the DB (http 500)
                            if (err) {
                                var response = {
                                    "msg": "Error de sistema obteniendo la cuenta Origen"
                                }
                                res.status(500);
                            //Treatment if the accounts stored in the DB or NOT.
                            }else{
                                if (body.length > 0) {
                                    
                                    var originCurrentBalance = body[0].balance;
                                    var ibanOrigen = body[0].iban;
                                    if (amount >= 0) {
                                        console.log("Realizando Ingreso de: " + amount );
                                        //console.log(body[0].balance);
                                        originUpdatedBalance = originCurrentBalance - amount;
                                    } 
                                    console.log("LastBalance: " + originCurrentBalance);
                                    console.log("UpdatedBalance: " + originUpdatedBalance);
                               

                                    var putBody = '{"$set":{"balance":' + originUpdatedBalance + '}}';

                                    httpClient.put("accounts?" + queryOriginAccount + "&" + mLabAPIKey, JSON.parse(putBody),
                                        function(err, resMLab, body) {
                                            console.log(JSON.parse(putBody));
                                            console.log("CUENTA ORIGEN: Balance actualizado con exito de: " + originCurrentBalance + " a " + originUpdatedBalance);
                                            //console.log(body[0].balance);
                                        }
                                    )
//**************************************************************** */
//Saving the value to the DB-Collection @accounts_movements to register the history of movements. 
//**************************************************************** */
                                    var postAccountsMovementsOrigin =  
                                        '{"Operation":"Transferencia Realizada",' 
                                                + '"ibanDestino":"' + ibanDestino  
                                                + '","account_id":' +  originAccount_id
                                                + ',"fecha":"' + fechaOperacion 
                                                + '","Concepto":"' + concepto 
                                                + '","CantidadModificada":"' + amount  
                                                + '","BalanceAnterior":' + originCurrentBalance 
                                                + ',"BalanceActual":' + originUpdatedBalance 
                                        + '}';


                                    console.log("CLIENTACCOUNTID: " + originAccount_id + "\nDESTINYACCOUNTID: " + destinyAccountId + "\nIBANORIGEN: " + ibanOrigen + "\nIBANDESTINO: " + ibanDestino + "\nCONCEPTO: " + concepto 
                                    + "\nCANTIDAD: " + amount + "\nDestinationCurrentBalance: " + destinationCurrentBalance +  "\nDestinationUpdatedBalance: " + destinationUpdatedBalance + "\nOriginCurrentBalance: " + originCurrentBalance +  "\nOriginUpdatedBalance: " + destinationUpdatedBalance );


                                    httpClient.post("accounts_movements?" +  "&" + mLabAPIKey, JSON.parse(postAccountsMovementsOrigin),
                                        function(err, resMLab, body) {
                                            console.log(JSON.parse(putBody));
                                        }
                                    )
        
        //**************************************************************** */
        //Saving the value to the DESTINY ACCOUNT->  DB-Collection @accounts_movements to register the history of movements. 
        //**************************************************************** */
                                                        
                                                        //Adding the transfer quantity to the destiny client Account. 

                                    var queryDestinationAccount = 'q={"id":' + destinyAccountId + '}'; 
                                    destinationUpdatedBalance = destinationCurrentBalance + amount;  
                                    console.log("LastDestinyBalance: " + destinationCurrentBalance);
                                    console.log("UpdatedDestinyBalance: " + destinationUpdatedBalance);
                                    var putBody = '{"$set":{"balance":' + destinationUpdatedBalance + '}}';


                                    httpClient.put("accounts?" + queryDestinationAccount + "&" + mLabAPIKey, JSON.parse(putBody),
                                        function(err, resMLab, body) {
                                            console.log(JSON.parse(putBody));
                                            console.log("CUENTA DESTINO: Balance actualizado con exito de: " + destinationCurrentBalance + " a " + destinationUpdatedBalance);
                                            //console.log(body[0].balance);
                                        }
                                    )

                                    var postAccountsMovementsDestination =  
                                    '{"Operation":"Transferencia Recibida",' 
                                            + '"ibanOrigen":"' + ibanOrigen  
                                            + '","account_id":' +  destinyAccountId
                                            + ',"fecha":"' + fechaOperacion 
                                            + '","Concepto":"' + concepto 
                                            + '","CantidadModificada":"' + amount  
                                            + '","BalanceAnterior":' + destinationCurrentBalance 
                                            + ',"BalanceActual":' + destinationUpdatedBalance 
                                    + '}';
                                    console.log("VALORES OBTENIDOS");
                                    console.log("-----------------");
                                    console.log("CLIENTACCOUNTID: " + originAccount_id + "\nDESTINYACCOUNTID: " + destinyAccountId + "\nIBANORIGEN: " + ibanOrigen + "\nIBANDESTINO: " + ibanDestino + "\nCONCEPTO: " + concepto 
                                    + "\nCANTIDAD: " + amount + "\nDestinationCurrentBalance: " + destinationCurrentBalance + "\nOriginCurrentBalance: " + originCurrentBalance);


                                    httpClient.post("accounts_movements?" +  "&" + mLabAPIKey, JSON.parse(postAccountsMovementsDestination),
                                        function(err, resMLab, body) {
                                            console.log(JSON.parse(putBody));
                                        }
                                    )



                                    var response = {
                                        "msg": "IBAN destino pertenece a esta entidad.",
                                        "Operation" : "Transferencia Realizada",
                                        "ibanDestino" : ibanDestino,
                                        "account_id":originAccount_id,
                                        "fecha": fechaOperacion,
                                        "Concepto": concepto,
                                        "CantidadModificada" :  amount,
                                        "BalanceAnterior" : originCurrentBalance,
                                        "BalanceActual" : originUpdatedBalance
                                    }
                                    //Treatment in case there weren't IBAN associated.
                                    
                                res.send(response);
                                }else{
                                    var response = {
                                        "msg": "Cuenta cliente Origen no encontrada.",
                                        
                                    }
                                    res.status(404);
                                }
                            }
                        }
                    )

                
                }else{

                console.log("ENTERING IN THE NO IBAN CLIENT ACCOUNT")
                //The destiny IBAN Acccount it's not from this bank. Sendind the money. 

                console.log("El IBAN: " + ibanDestino + " destino no pertenece a esta entidad.");
                console.log("Enviando Transferencia");
                
                                        
                // **************************************************
                //  Getting and updating the Origin account.
                // ************************************************** 

                    var queryOriginAccount = 'q={"id":' + originAccount_id + '}';

                    console.log("llegando al tratamiento de la cuenta de destino.");
                    console.log("llegando al tratamiento de la cuenta de destino.: Query-> " + queryOriginAccount);
                    
                    httpClient.get("accounts?" + queryOriginAccount + "&" + mLabAPIKey,
                        function(err, resMlab, body) {
                            //Treatment for system error, getting the query to the DB (http 500)
                            if (err) {
                                var response = {
                                    "msg": "Error de sistema obteniendo la cuenta Origen"
                                }
                                res.status(500);
                            //Treatment if the accounts stored in the DB or NOT.
                            }else{
                                if (body.length > 0) {
                                    
                                    var originCurrentBalance = body[0].balance;
                                    var ibanOrigen = body[0].iban;
                                    if (amount >= 0) {
                                        console.log("Realizando Ingreso de: " + amount );
                                        //console.log(body[0].balance);
                                        originUpdatedBalance = originCurrentBalance - amount;
                                    } 
                                    console.log("LastBalance: " + originCurrentBalance);
                                    console.log("UpdatedBalance: " + originUpdatedBalance);
                                    

                                    var putBody = '{"$set":{"balance":' + originUpdatedBalance + '}}';
                                    
                                    httpClient.put("accounts?" + queryOriginAccount + "&" + mLabAPIKey, JSON.parse(putBody),
                                        function(err, resMLab, body) {
                                            console.log(JSON.parse(putBody));
                                            console.log("Balance Cuenta Origen actualizado con exito de: " + originCurrentBalance + " a " + originUpdatedBalance);
                                            //console.log(body[0].balance);
                                        }
                                    )
        //**************************************************************** */
        //Saving the value to the DB-Collection @accounts_movements to register the history of movements. 
        //**************************************************************** */
                                        var postAccountsMovementsOrigin =  
                                        '{"Operation":"Transferencia Realizada",' 
                                                + '"ibanDestino":"' + ibanDestino  
                                                + '","account_id":' +  originAccount_id
                                                + ',"fecha":"' + fechaOperacion 
                                                + '","Concepto":"' + concepto 
                                                + '","CantidadModificada":"' + amount  
                                                + '","BalanceAnterior":' + originCurrentBalance 
                                                + ',"BalanceActual":' + originUpdatedBalance 
                                        + '}';


                                    console.log("CLIENTACCOUNTID: " + originAccount_id + "\nDESTINYACCOUNTID: " + destinyAccountId + "\nIBANORIGEN: " + ibanOrigen + "\nIBANDESTINO: " + ibanDestino + "\nCONCEPTO: " + concepto 
                                    + "\nCANTIDAD: " + amount + "\nDestinationCurrentBalance: " + destinationCurrentBalance + "\nOriginCurrentBalance: " + originCurrentBalance);


                                    httpClient.post("accounts_movements?" +  "&" + mLabAPIKey, JSON.parse(postAccountsMovementsOrigin),
                                        function(err, resMLab, body) {
                                            console.log(JSON.parse(postAccountsMovementsOrigin));
                                        }
                                    )
        
                                    

                                    var response = {
                                        "msg" : "IBAN destino NO pertenece a esta entidad. Enviando transferencia.",
                                        "Operation" : "Transferencia Realizada",
                                        "ibanDestino" : ibanDestino,
                                        "account_id":originAccount_id,
                                        "fecha": fechaOperacion,
                                        "Concepto" : concepto,
                                        "CantidadModificada" :  amount,
                                        "BalanceAnterior" : originCurrentBalance,
                                        "BalanceActual" : originUpdatedBalance

                                    }
                                    res.send(response);
                                    

                                }else{
                                    var response = {
                                        "msg": "Cuenta Origen Cliente no encontrada.",
                                        
                                    }
                                    res.status(404);
                                }
                            }
                        }
                    )

                }
            }
        }) 
   
    //IN CASE THE AMOUNT IS <0.
    }else{
        
        var response = {
            
                "msg" : "La cantidad ingresada en menor a 0. Por favor, introduzca un valor positivo.",
                
            
        }
        console.log("Treatment With the EXTERNAL IBAN ACCOUNT");
        res.send(response);
    }




}//End Funcion


                

  
 
    





    
                        

module.exports.getAccountById = getAccountById;
module.exports.newMov = newMov;
module.exports.listMov = listMov;
module.exports.transferMov = transferMov;