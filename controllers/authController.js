/****************************************************************************************                         
                                START: Environment Initialization
 ***************************************************************************************/
// INFO: @requestjson -> Simplifying JSON requests.
// INFO: @baseMLABURL -> URL for the connection to MLAB
// INFO: @mLabAPIKey -> APIKEY for the connection to MLAB.
// INFO: @crypt -> Charging crypt for the creation of user passwords creation (hashed) */

const requestJSON = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjb9ed/collections/";
const crypt = require('../crypt');
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

/****************************************************************************************                         
                                END: Environment Initialization
****************************************************************************************/


/****************************************************************************************                         
                                START: FUNCTIONS
 ***************************************************************************************/

 
/*Function @login Treating the logging user. 
        INPUT: 
        @"email": EMAIL
        @"password": Clear Password. 
        ACTIONS: 
        Checking if the user is stored in the MLAB Database and Checking that the password Hashed is right.  
        If the logging is right, added a property to the MLAB Database -> Logged:True.
        RETURN:
        Returning  result of user login.

*/

function login(req, res) {

    //Inside the function apitechu/login
    console.log("POST /apitechu/login");
    //Checking the body.data received.
    console.log("El Email del usuario es: " + req.body.email);
    console.log("El Password del usuario es: " + req.body.password);
    //Storing Email and Password in variables email & password for later treatment.
    var email = req.body.email;
    var password = req.body.password;

    //Creating the connection to the DB (MLAB) 
    var httpClient = requestJSON.createClient(baseMlabURL);
    console.log('Conexión cliente creada a:\n ', baseMlabURL);
    //Generate the query for the MLAB user query.
    var query = 'q={"email":"' + email + '"}';
    var queryTotal = baseMlabURL + "user?" + query + "&" + mLabAPIKey;
    console.log(query);
    console.log(queryTotal);

    //Checking if the user is previously created in the DB. Checking @body.email with the email stored in the DB
    httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMlab, body) {
            var userfinded = false;
            //Treatment for system error, getting the query to the DB (http 500)
            if (err) {
                var response = {
                    msg: "Error del sistema status(500)"

                }
                res.status(500);
            //Treatment in case the user is stored in the DB or NOT. 
            } else {

                if (body.length > 0) {
                    //Confirm that the user was finded. @userfinded = true.
                    userfinded = true;
                    response = {
                        msg: "Usuario encontrado en BD."
                    };
                    user = body[0];
                    //Case the user was not found. @userfinded = false.
                } else {
                    response = {
                        msg: "Usuario no encontrado en BD..."
                    };
                    res.status(404);

                }
            }
            
            //User finded in DB-> Checking password crypt.checkPassword.
            if (userfinded) {
                if (crypt.checkPassword(password, user.password)) {
                    console.log("password ok");
                    var loggedUserId = user.id;
                    user.logged = true;
                    console.log("Logged in user with id " + user.id);

                    //Set the value logged = true to the DATABASE. 
                    var putBody = '{"$set":{"logged":true}}';
                    httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                        function(err, resMLab, body) {
                            console.log("Usuario logado con exito");
                        }
                    )
                }
            }
            //Return message in case of the Login was right or not.
            var msg = loggedUserId ?
                "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";
            
            //Setting response
            var response = {
                "mensaje": msg,
                "idUsuario": loggedUserId
            };
            //Sending response (res)
            console.log(response);
            res.send(response);

        }
    )
}


/*Function @logout Treating the user logout. 
        INPUT: 
        @"user": /:id
        ACTIONS: 
        Logout of the user passwd by parameter.
        RETURN:
        Returning message confirmation of the Logout.

*/
function logout(req, res) {
    //Checking we are inside the function.
    console.log('POST /apitechu/logout/:id');
     //Creating database connection.
    var httpClient = requestJSON.createClient(baseMlabURL);
    console.log('Conexión cliente creada a:\n ', baseMlabURL);

    //Saving the params received into a variable @id.
    var id = req.params.id;
    console.log(req.params.id);

    //Setting the query for getting the user DB.
    var query = 'q={"id":' + id + '}';
    httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMlab, body) {
            //In case of system error. 
            if (err) {
                var response = {
                    msg: "Error del sistema status(500)"
                }
                res.status(500);
            //In case of user found.
            } else {
                if (body.length > 0) {
                    response = "Usuario encontrado en BD."
                    user = body[0];
            //In case of user not found.
                } else {
                    response = {
                        msg: "Usuario no encontrado en BD"
                    }
                    res.status(404);
                }
            }

            //If the user is logged execute the logout. 
            if (user.logged === true) {
                console.log("Usuario esta actualmente logado. Haciendo Logout...");
                //Changing the value of logged:true to unset. -> User is not already logged.
                var putBody = '{"$unset":{"logged":""}}';
                httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                    function(errPUT, resMLABPUT, bodyPUT) {
                        console.log("Usuario persistido con exito en BD");
                    })
                console.log("Logout user with ID: " + user.id);
                
                //Setting the response message. 
                var loggedOutUserID = user.id;
                var msg = loggedOutUserID ?
                    "Logout correcto" : "Logout incorrecto";

                var response = {
                    "msg": msg,
                    "idUsuario": loggedOutUserID
                };

                res.send(response);
            }

        });





}
module.exports.login = login;
module.exports.logout = logout;